package main

import (
   "os"
   "path/filepath"

   "github.com/charmbracelet/log"
   "github.com/spf13/cobra"
   "github.com/spf13/viper"

   "gitlab.com/avocet-tools/wulfila"
)

const Version = "0.1.0"

var cmd = cobra.Command{
   Use: "wulfila",
   Short: "Tool for Language Creation and Administration",
   Run: func (_ *cobra.Command, args []string){
      wulfila.Run(args)
   },
   Version: Version,
}

func init(){
   log.Debug("Finding User Home Directory")
   home, err := os.UserHomeDir()
   if err != nil {
      log.Fatal("Unable to set user home directory")
   }
   log.Debug("User Home Set: ", home)
   viper.SetDefault("wulfila.home", home)

   // Finding Default Cache Directory
   log.Debug("Finding default cache directory")
   cache := filepath.Join(home, ".local/var/wulfila")
   viper.SetDefault("wulfila.cache", cache)
   log.Debug("Ensuring Cache Directory")
   _, err = os.Stat(cache)
   if os.IsNotExist(err){
      log.Info("Creating Cache Directory: ", cache)
      os.MkdirAll(cache, 0444)
   }
   log.Debug("Cache Directory Set: ", cache)
}

func main(){
   cmd.Execute()
}





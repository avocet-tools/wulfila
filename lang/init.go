package lang

import (
   "os"
   "path/filepath"

   "github.com/charmbracelet/log"
   "github.com/spf13/viper"
   "gopkg.in/yaml.v3"
)


func findLangs() []string {
   cache := viper.GetString("wulfila.cache")
   fis, err := os.ReadDir(cache)
   if err != nil {
      log.Fatal("Unable to read Directory:\n", err)
   }
   lfs := []string{}
   for _, fi := range fis {
      lfs = append(lfs, filepath.Join(cache, fi.Name()))
   }

   return lfs
}



func LoadLangs(args []string) map[string]*Language {
   m := make(map[string]*Language)
   for _, f := range findLangs() {
      lang := &Language{}
      con, err := os.ReadFile(f)
      if err != nil {
         log.Error("Issue reading file: ", f, "\n", err)
         continue
      }
      err = yaml.Unmarshal(con, lang)
      if err != nil {
         log.Error("Issue unmarshaling YAML: ", f, "\n", err)
         continue
      }
      m[lang.Key] = lang
   }
   return m
}


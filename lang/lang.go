package lang

type Language struct {
   Key string `json:"key" yaml:"key" bson:"key"`
   Name string `json:"name" yaml:"name" bson:"name"`
   Abbr string `json:"abbr" yaml:"abbr" bson:"abbr"`
}
